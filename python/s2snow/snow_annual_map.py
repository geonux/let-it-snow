#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2019 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Let-it-snow (LIS)
#
#     https://gitlab.orfeo-toolbox.org/remote_modules/let-it-snow
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import os
import os.path as op
import shutil
from datetime import timedelta

import gdal
# OTB Applications
import otbApplication as otb

# Import python decorators for the different needed OTB applications
from s2snow.app_wrappers import band_math, get_app_output, super_impose, band_mathX, gap_filling
from s2snow.snow_product_parser import load_snow_product
from s2snow.utils import datetime_to_str
from s2snow.utils import write_list_to_file, read_list_from_file

# Build gdal option to generate maks of 1 byte using otb extended filename
# syntaxx
GDAL_OPT = "?&gdal:co:NBITS=1&gdal:co:COMPRESS=DEFLATE"

LABEL_NO_SNOW = "0"
LABEL_SNOW = "100"
LABEL_CLOUD = "205"
LABEL_NO_DATA = "255"
LABEL_NO_DATA_OLD = "254"

def compute_SCD(binary_snow_mask_list, multitemp_cloud_vrt, input_dates_filename, output_dates_filename, output_dates,
                snow_coverage_duration, multitemp_snow_vrt, gapfilled_timeserie, multitemp_snow100,
                multitemp_snow100_gapfilled, ram):
    logging.info("Start compute_SCD")

    # build snow mask vrt
    logging.info("Building multitemp snow mask vrt")
    logging.info("snow vrt: {}".format(multitemp_snow_vrt))

    gdal.BuildVRT(multitemp_snow_vrt,
                  binary_snow_mask_list,
                  separate=True,
                  srcNodata='None')

    # multiply by 100 for the temporal interpolation
    logging.info("Scale by 100 multitemp snow mask vrt")
    bandMathXApp = band_mathX([multitemp_snow_vrt],
                              multitemp_snow100,
                              "im1 mlt 100",
                              ram,
                              otb.ImagePixelType_uint8)
    bandMathXApp.ExecuteAndWriteOutput()
    bandMathXApp = None

    # gap filling the snow timeserie
    logging.info("Gap filling the snow timeserie")
    app_gap_filling = gap_filling(multitemp_snow100,
                                  multitemp_cloud_vrt,
                                  multitemp_snow100_gapfilled + "?&gdal:co:COMPRESS=DEFLATE",
                                  input_dates_filename,
                                  output_dates_filename,
                                  ram,
                                  otb.ImagePixelType_uint8)
    # @TODO the mode is for now forced to DEBUG in order to generate img on disk
    # img_in = get_app_output(app_gap_filling, "out", mode)
    # if mode == "DEBUG":
    # shutil.copy2(gapfilled_timeserie, path_out)
    # app_gap_filling = None
    img_in = get_app_output(app_gap_filling, "out", "DEBUG")
    app_gap_filling = None
    # threshold to 0 or 1
    logging.info("Round to binary series of snow occurrence")
    bandMathXApp = band_mathX([img_in],
                              gapfilled_timeserie + GDAL_OPT,
                              "(im1 mlt 2) dv 100",
                              ram,
                              otb.ImagePixelType_uint8)
    bandMathXApp.ExecuteAndWriteOutput()
    bandMathXApp = None

    # generate the annual map
    logging.info("Generate Snow coverate duration: {}".format(snow_coverage_duration))
    band_index = list(range(1, len(output_dates) + 1))
    logging.debug("Bande index: {}".format(band_index))
    expression = "+".join(["im1b" + str(i) for i in band_index])
    logging.debug("expression: {}".format(expression))
    bandMathApp = band_math([gapfilled_timeserie],
                            snow_coverage_duration  + "?&gdal:co:COMPRESS=DEFLATE",
                            expression,
                            ram,
                            otb.ImagePixelType_uint16)
    bandMathApp.ExecuteAndWriteOutput()
    bandMathApp = None
    logging.info("End compute_SCD")


def compute_CCD(binary_cloud_mask_list, ccd_file_path, multitemp_cloud_vrt, ram):
    logging.info("Start compute_CCD")

    # build cloud mask vrt
    logging.info("Building multitemp cloud mask vrt")
    logging.info("cloud vrt: {}".format(multitemp_cloud_vrt))
    gdal.BuildVRT(multitemp_cloud_vrt,
                  binary_cloud_mask_list,
                  separate=True,
                  srcNodata='None')

    # generate the summary map
    logging.info("Generate Cloud_Occurence: {}".format(ccd_file_path))
    band_index = list(range(1, len(binary_cloud_mask_list) + 1))
    logging.debug("bande index: {}".format(band_index))
    expression = "+".join(["im1b" + str(i) for i in band_index])
    logging.debug("expression: {}".format(expression))
    bandMathApp = band_math([multitemp_cloud_vrt],
                            ccd_file_path,
                            expression,
                            ram,
                            otb.ImagePixelType_uint16)
    bandMathApp.ExecuteAndWriteOutput()
    bandMathApp = None
    logging.info("End compute_CCD")

def convert_snow_masks_into_binary_cloud_masks(path_out, ram, product_dict):
    logging.info("Start convert_snow_masks_into_binary_cloud_masks")
    # convert the snow masks into binary cloud masks
    expression = "im1b1==" + LABEL_CLOUD + "?1:(im1b1==" + LABEL_NO_DATA + "?1:(im1b1==" + LABEL_NO_DATA_OLD + "?1:0))"
    binary_cloud_mask_list = convert_mask_list(path_out, product_dict, expression, "cloud", ram,
                                               mask_format=GDAL_OPT)
    logging.debug("Binary cloud mask list:")
    logging.debug(binary_cloud_mask_list)
    logging.info("Start convert_snow_masks_into_binary_cloud_masks")
    return binary_cloud_mask_list


def convert_snow_masks_into_binary_snow_masks(path_out, ram, product_dict):
    logging.info("Start convert_snow_masks_into_binary_snow_masks")
    # convert the snow masks into binary snow masks
    expression = "(im1b1==" + LABEL_SNOW + ")?1:0"
    binary_snow_mask_list = convert_mask_list(path_out, product_dict, expression, "snow", ram,
                                              mask_format=GDAL_OPT)
    logging.debug("Binary snow mask list:")
    logging.debug(binary_snow_mask_list)
    logging.info("End convert_snow_masks_into_binary_snow_masks")
    return binary_snow_mask_list


def merge_product_at_same_date(path_out, product_dict, ram):
    logging.info("Start merge_product_at_same_date")
    merge_product_dict = {}
    for key in list(product_dict.keys()):
        if len(product_dict[key]) > 1:
            merged_mask = op.join(path_out, key + "_merged_snow_product.tif")
            merge_masks_at_same_date(product_dict[key],
                                     merged_mask,
                                     LABEL_SNOW,
                                     ram)
            merge_product_dict[key] = merged_mask
        else:
            merge_product_dict[key] = product_dict[key][0].get_snow_mask()
    logging.info("End merge_product_at_same_date")
    return merge_product_dict


def compute_output_dates(date_start, date_stop, output_dates_file_path):
    logging.info("Start compute_output_dates")
    output_dates = []
    if op.exists(output_dates_file_path):
        logging.debug("Read output_date_file : {}".format(output_dates_file_path))
        output_dates = read_list_from_file(output_dates_file_path)
    else:
        logging.debug("Compute output_dates from {} to {}".format(date_start, date_stop))
        tmp_date = date_start
        while tmp_date <= date_stop:
            output_dates.append(datetime_to_str(tmp_date))
            tmp_date += timedelta(days=1)
        write_list_to_file(output_dates_file_path, output_dates)
    logging.info("End compute_output_dates")
    return output_dates


def load_densification_products(date_margin, date_start, date_stop, densification_path_list, path_tmp, product_dict,
                                ram):
    logging.info("Start load_densification_products")
    # load densification snow products
    densification_product_dict = load_products(date_start, date_stop, date_margin, densification_path_list, None,
                                               None)
    logging.debug("Densification product dict:")
    logging.debug(densification_product_dict)

    # Get the footprint of the first snow product
    s2_footprint_ref = product_dict[list(product_dict.keys())[0]][0].get_snow_mask()

    if densification_product_dict:
        # Reproject the densification products on S2 tile before going further
        for densifier_product_key in list(densification_product_dict.keys()):
            for densifier_product in densification_product_dict[densifier_product_key]:
                original_mask = densifier_product.get_snow_mask()
                reprojected_mask = op.join(path_tmp,
                                           densifier_product.product_name + "_reprojected.tif")
                if not os.path.exists(reprojected_mask):
                    super_impose_app = super_impose(s2_footprint_ref,
                                                    original_mask,
                                                    reprojected_mask,
                                                    "nn",
                                                    int(LABEL_NO_DATA),
                                                    ram,
                                                    otb.ImagePixelType_uint8)
                    super_impose_app.ExecuteAndWriteOutput()
                    super_impose_app = None
                densifier_product.snow_mask = reprojected_mask
                logging.debug(densifier_product.snow_mask)

            # Add the products to extend the product_dict
            if densifier_product_key in list(product_dict.keys()):
                product_dict[densifier_product_key].extend(densification_product_dict[densifier_product_key])
            else:
                product_dict[densifier_product_key] = densification_product_dict[densifier_product_key]

            logging.debug(product_dict[densifier_product_key])
    else:
        logging.warning("No Densifying candidate product found!")
    logging.info("End load_densification_products")


def load_products(date_start, date_stop, date_margin, snow_products_list, tile_id=None, product_type=None):
    logging.info("Start load_products")
    logging.info("Parsing provided snow products list")
    product_dict = {}
    search_start_date = date_start - date_margin
    search_stop_date = date_stop + date_margin
    for product_path in snow_products_list:
        try:
            product = load_snow_product(str(product_path))
            logging.info(str(product))
            current_day = datetime_to_str(product.acquisition_date)
            test_result = True
            if search_start_date > product.acquisition_date or \
                    search_stop_date < product.acquisition_date:
                test_result = False
            if (tile_id is not None) and (tile_id not in product.tile_id):
                test_result = False
            if (product_type is not None) and (product_type not in product.platform):
                test_result = False
            if test_result:
                if current_day not in list(product_dict.keys()):
                    product_dict[current_day] = [product]
                else:
                    product_dict[current_day].append(product)
                logging.info("Keeping: {}".format(str(product)))
            else:
                logging.warning("Discarding: {}".format(str(product)))
        except Exception:
            logging.error("Unable to load product: {}".format(product_path))
    logging.debug("Product dictionnary:")
    logging.debug(product_dict)
    logging.info("End load_products")
    return product_dict


def convert_mask_list(path_tmp, resulting_snow_mask_dict, expression, type_name, ram, mask_format=""):
    binary_mask_list = []
    for mask_date in sorted(resulting_snow_mask_dict):
        binary_mask = op.join(path_tmp,
                              mask_date + "_" + type_name + "_binary.tif")
        binary_mask = extract_binary_mask(resulting_snow_mask_dict[mask_date],
                                          binary_mask,
                                          expression,
                                          ram,
                                          mask_format)
        binary_mask_list.append(binary_mask)
    return binary_mask_list


def extract_binary_mask(mask_in, mask_out, expression, ram, mask_format=""):
    bandMathApp = band_math([mask_in],
                            mask_out + mask_format,
                            expression,
                            ram,
                            otb.ImagePixelType_uint8)
    bandMathApp.ExecuteAndWriteOutput()
    return mask_out


def merge_masks_at_same_date(snow_product_list, merged_snow_product, threshold=100, ram=None):
    """ This function implement the fusion of multiple snow mask

    Keyword arguments:
    snow_product_list -- the input mask list
    merged_snow_product -- the output filepath
    threshold -- the threshold between valid <= invalid data
    ram -- the ram limitation (not mandatory)
    """
    logging.info("Merging products into {}".format(merged_snow_product))

    # the merging is performed according the following selection:
    #   if img1 < threshold use img1 data
    #   else if img2 < threshold use img2 data
    #   else if imgN < threshold use imgN data
    # the order of the images in the input list is important:
    #   we expect to have first the main input products
    #   and then the densification products
    img_index = list(range(1, len(snow_product_list) + 1))
    expression_merging = "".join(["(im" + str(i) + "b1<=" + str(threshold) + "?im" + str(i) + "b1:" for i in img_index])
    expression_merging += "im" + str(img_index[-1]) + "b1"
    expression_merging += "".join([")" for i in img_index])

    img_list = [i.get_snow_mask() for i in snow_product_list]
    bandMathApp = band_math(img_list,
                            merged_snow_product,
                            expression_merging,
                            ram,
                            otb.ImagePixelType_uint8)
    bandMathApp.ExecuteAndWriteOutput()
    bandMathApp = None
